package com.example.employee.externalapi;

import com.example.employee.externalapi.model.DaysNo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class LegalDayOffServiceExtImplTest {
//  @Autowired
  private RestTemplate restTemplate;

//  @Value("${ext.api.endpoint.legal-day-off.days-no}")
  private String daysNoEndpoint;

  //@Test
  public void getDaysNo() {
    String endPoint = String.format(daysNoEndpoint, "01-01-2018", "31-12-2018");

    restTemplate.getForObject(endPoint, DaysNo.class);
  }
}