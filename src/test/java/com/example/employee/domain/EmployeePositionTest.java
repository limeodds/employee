package com.example.employee.domain;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class EmployeePositionTest {

  @Test
  public void test_code_unique() {
    //given
    long noOfValues = EmployeePosition.values().length;
    long noOfUniqueCodes = Stream.of(EmployeePosition.values()).map(EmployeePosition::getCode).collect(toSet()).size();

    //then
    Assertions.assertThat(noOfValues).isEqualTo(noOfUniqueCodes);
  }

}