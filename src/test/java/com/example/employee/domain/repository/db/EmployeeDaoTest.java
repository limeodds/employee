package com.example.employee.domain.repository.db;

import com.example.employee.repository.db.EmployeeDao;
import com.example.employee.domain.EmployeePosition;
import com.example.employee.repository.db.entity.EmployeeEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class EmployeeDaoTest {
  private String managerUserName = "jeanlp";
  private EmployeeEntity manager = new EmployeeEntity(null, managerUserName, "Jean-Luc", "Picard",
                                                      EmployeePosition.IT_MANAGER.name(), null, 100, 25);

  private EmployeeEntity itst = new EmployeeEntity(null, "willo", "Willy", "Wonka",
                                                   EmployeePosition.SYSTEM_ENGINEER.name(), null, 25, 2);

  @Autowired
  private EmployeeDao employeeDao;

  @Before
  public void setup() {
    EmployeeEntity added = employeeDao.add(manager);
    manager.setId(added.getId());

    itst.setManagerId(manager.getId());

    added = employeeDao.add(itst);
    itst.setId(added.getId());
  }


  @Test
  public void findBy() {
    EmployeeEntity entity = employeeDao.findBy(managerUserName);

    assertThat(entity.getUserName()).isEqualToIgnoringCase(managerUserName);
  }

  @After
  public void cleanup() {
    employeeDao.deleteById(manager.getId());
    employeeDao.deleteById(itst.getId());
  }
}