package com.example.employee.domain.repository.converter;

import com.example.employee.repository.converter.VacationConverter;
import com.example.employee.domain.Vacation;
import com.example.employee.domain.VacationRequestStatus;
import com.example.employee.repository.db.entity.VacationEntity;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

public class VacationConverterTest {
  private static final Long id = 123L;
  private static final String userName = "spiderman";
  private static final LocalDate startDate = LocalDate.of(2020, Month.DECEMBER, 1);
  private static final LocalDate endDate = LocalDate.of(2020, Month.DECEMBER, 31);
  private static final VacationRequestStatus status = VacationRequestStatus.APPROVED;

  @Test
  public void toDomain() {
    //given
    VacationEntity entity = VacationEntity.builder()
                                          .id(id)
                                          .userName(userName)
                                          .startDate(startDate)
                                          .endDate(endDate)
                                          .status(status.name())
                                          .build();
    //when
    Vacation domainObj = VacationConverter.toDomain(entity);
    //then
    assertThat(domainObj.getId()).isEqualTo(id);
    assertThat(domainObj.getUserName()).isEqualTo(userName);
    assertThat(domainObj.getStartDate()).isEqualTo(startDate);
    assertThat(domainObj.getEndDate()).isEqualTo(endDate);
    assertThat(domainObj.getStatus()).isEqualTo(status);
  }

  @Test
  public void toDb() {
    //given
    Vacation domainObj = Vacation.builder()
                                 .id(id)
                                 .userName(userName)
                                 .startDate(startDate)
                                 .endDate(endDate)
                                 .status(status)
                                 .build();
    //when
    VacationEntity entity = VacationConverter.toDb(domainObj);
    //then
    assertThat(entity.getId()).isEqualTo(id);
    assertThat(entity.getUserName()).isEqualTo(userName);
    assertThat(entity.getStartDate()).isEqualTo(startDate);
    assertThat(entity.getEndDate()).isEqualTo(endDate);
    assertThat(entity.getStatus()).isEqualTo(status.name());
  }
}