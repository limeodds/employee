package com.example.employee.domain.repository.db;

import com.example.employee.repository.db.VacationDao;
import com.example.employee.domain.VacationRequestStatus;
import com.example.employee.repository.db.entity.VacationEntity;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class VacationDaoTest {
  private final String userName = "qwerty";
  private final List<VacationEntity> vacations = Arrays.asList(new VacationEntity(null, userName,
                                                                                  LocalDate.of(1930, Month.JANUARY, 1),
                                                                                  LocalDate.of(1930, Month.JANUARY, 31),
                                                                                  VacationRequestStatus.VALIDATED.name()));

  @Autowired
  private VacationDao vacationDao;

  @Before
  public void setup() {
    vacations.forEach(vacation -> {
      VacationEntity saved = vacationDao.add(vacation);
      vacation.setId(saved.getId());
    });
  }

  @Test
  public void findBy_username() {
    List<VacationEntity> foundVacations = vacationDao.findBy(userName);

    Assertions.assertThat(foundVacations).hasSize(1);
  }

  @Test
  public void updateStatus() {
    //given
    String status = VacationRequestStatus.VALIDATED.name();
    VacationEntity entity = vacations.get(0);

    //when
    vacationDao.updateStatus(entity.getId(), status);

    //then
    Assertions.assertThat(vacationDao.findById(entity.getId()).getStatus()).isEqualToIgnoringCase(status);
  }

  @After
  public void cleanup() {
    vacations.forEach(vacation -> vacationDao.deleteById(vacation.getId()));
  }
}