CREATE DATABASE vacation DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
CREATE USER 'test'@'localhost' IDENTIFIED BY 'test123';
GRANT ALL PRIVILEGES ON vacation.* TO 'test'@'localhost';

drop table if exists T_EMPLOYEE;
create table T_EMPLOYEE (
  ID INT not null AUTO_INCREMENT,
  USERNAME varchar(50) not null UNIQUE,
  FIRST_NAME varchar(50) not null,
  LAST_NAME varchar(50) not null,
  POSITION varchar(50) not null,
  MANAGER_ID INT,
  TOTAL_DAYS_OFF int not null,
  AVAILABLE_DAYS_OFF int not null,
  PRIMARY KEY (id),
  FOREIGN KEY fk_manager(MANAGER_ID)  REFERENCES T_EMPLOYEE(id) ON DELETE SET NULL);

drop table if exists T_VACATION;
create table T_VACATION (
  ID INT not null AUTO_INCREMENT,
  USERNAME varchar(50) not null,
  START_DATE DATE not null,
  END_DATE DATE not null,
  status varchar(50) not null,
  PRIMARY KEY (id));