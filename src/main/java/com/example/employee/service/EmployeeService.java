package com.example.employee.service;

import com.example.employee.domain.Employee;

public interface EmployeeService {
  Employee findBy(Long id);

  Employee findBy(String userName);

  Employee add(Employee employee);

  void update(Employee employee);
}
