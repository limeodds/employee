package com.example.employee.service;

import com.example.employee.externalapi.model.DaysNo;

import java.time.LocalDate;

public interface LegalDayOffService {
  DaysNo getDaysNo(LocalDate startDate, LocalDate endDate);
}
