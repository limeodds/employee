package com.example.employee.service.impl;

import com.example.employee.domain.Vacation;
import com.example.employee.domain.VacationRequestStatus;
import com.example.employee.repository.VacationRepository;
import com.example.employee.service.VacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VacationServiceImpl implements VacationService {
  @Autowired
  private VacationRepository vacationRepository;

  @Override
  @Transactional
  public Vacation findById(Long id) {
    return vacationRepository.findById(id);
  }

  @Override
  @Transactional
  public List<Vacation> findBy(String userName) {
    return vacationRepository.findBy(userName);
  }

  @Override
  @Transactional
  public Vacation add(Vacation vacation) {
    return vacationRepository.add(vacation);
  }

  @Override
  @Transactional
  public void updateStatus(Long id, VacationRequestStatus status) {
    vacationRepository.updateStatus(id, status);
  }
}
