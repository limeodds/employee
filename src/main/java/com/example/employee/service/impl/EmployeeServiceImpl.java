package com.example.employee.service.impl;

import com.example.employee.domain.Employee;
import com.example.employee.repository.EmployeeRepository;
import com.example.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
  @Autowired
  private EmployeeRepository employeeRepository;

  @Override
  public Employee findBy(Long id) {
    return employeeRepository.findBy(id);
  }

  @Override
  public Employee findBy(String userName) {
    return employeeRepository.findBy(userName);
  }

  @Override
  public Employee add(Employee employee) {
    return employeeRepository.add(employee);
  }

  @Override
  public void update(Employee employee) {
    employeeRepository.update(employee);
  }
}
