package com.example.employee.domain.converter;

import com.example.employee.domain.Employee;
import com.example.employee.domain.EmployeePosition;
import com.example.employee.domain.json.EmployeeJson;
import com.example.employee.domain.json.EmployeeJsonInfo;

public class EmployeeJsonConverter {
  public static Employee toDomain(EmployeeJson jsonObj) {
    return Employee.builder()
                   .userName(jsonObj.getUserName())
                   .firstName(jsonObj.getFirstName())
                   .lastName(jsonObj.getLastName())
                   .position(EmployeePosition.fromCode(jsonObj.getPositionCode()))
                   .managerId(jsonObj.getManagerId())
                   .totalDaysOff(jsonObj.getTotalDaysOff())
                   .build();
  }

  public static EmployeeJsonInfo toJsonInfo(Employee obj) {
    return EmployeeJsonInfo.builder()
                           .id(obj.getId())
                           .userName(obj.getUserName())
                           .firstName(obj.getFirstName())
                           .lastName(obj.getLastName())
                           .positionDescription(obj.getPosition().getDescription())
                           .managerId(obj.getManagerId())
                           .totalDaysOff(obj.getTotalDaysOff())
                           .availableDaysOff(obj.getAvailableDaysOff())
                           .build();
  }
}
