package com.example.employee.domain.converter;

import com.example.employee.domain.DateUtils;
import com.example.employee.domain.Vacation;
import com.example.employee.domain.json.VacationJson;

public class VacationJsonConverter {
  public static VacationJson toJson(Vacation obj) {
    return VacationJson.builder()
                       .userName(obj.getUserName())
                       .startDate(DateUtils.toString(obj.getStartDate()))
                       .endDate(DateUtils.toString(obj.getEndDate()))
                       .status(obj.getStatus().name())
                       .build();
  }

}
