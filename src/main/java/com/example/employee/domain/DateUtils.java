package com.example.employee.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {
  public static final String dateTimePattern = "dd-MM-yyyy";
  public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimePattern);

  public static LocalDate toLocalDate(String value) {
    return LocalDate.parse(value, dateTimeFormatter);
  }

  public static String toString(LocalDate value) {
    return dateTimeFormatter.format(value);
  }
}
