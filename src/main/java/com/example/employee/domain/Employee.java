package com.example.employee.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class Employee {
  private Long id;
  private String userName;
  private String firstName;
  private String lastName;
  private EmployeePosition position;

  /** Can be null */
  private Long managerId;

  private int totalDaysOff;
  private int availableDaysOff;

  public String getFullName() {
    return firstName + " " + lastName;
  }
}
