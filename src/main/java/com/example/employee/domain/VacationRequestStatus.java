package com.example.employee.domain;

public enum VacationRequestStatus {
  SUBMITED,//user submited Vacation Request to the Manager
  VALIDATED, //system automatically validated request
  APPROVED, //Manager approved Vacation Request
  REJECTED //Manager rejected Vacation Request
}
