package com.example.employee.domain.json;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor @Builder
public class EmployeeJsonInfo {
  private Long id;

  private String userName;
  private String firstName;
  private String lastName;
  private String positionDescription;

  /** Can be null */
  private Long managerId;
  private String managerUsername;
  private String managerFullName;

  private int totalDaysOff;
  private int availableDaysOff;
}
