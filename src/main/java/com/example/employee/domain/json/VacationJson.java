package com.example.employee.domain.json;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VacationJson {
  private String userName;
  private String startDate;
  private String endDate;
  private String status;
}
