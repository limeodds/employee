package com.example.employee.domain.json;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor @Builder
public class EmployeeJson {
  private Long id;

  private String userName;
  private String firstName;
  private String lastName;
  private String positionCode;

  /** Can be null */
  private Long managerId;

  private int totalDaysOff;
}
