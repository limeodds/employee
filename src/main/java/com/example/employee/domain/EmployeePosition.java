package com.example.employee.domain;

import lombok.Getter;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@Getter
public enum EmployeePosition {
  TESTER("Tester", "T"),
  TECHNICAL_ANALYST("Technical Analyst", "TA"),
  TECHNICAL_ANALYSIS_MANAGER("Technical Analysis Manager", "TAM"),
  SYSTEM_ENGINEER("System Engineer", "SE"),
  SALES_REPRESENTATIVE("Sales Representative", "SRP"),
  SALES_AREA_MANAGER("Sales Area Manager", "SAM"),
  QUALITY_ASSURANCE_MANAGER("Quality Assurance Manager", "QAM"),
  IT_MANAGER("IT Manager", "ITM"),
  BUSINESS_ANALYST("Business Analyst", "BA"),
  BUSINESS_ANALYSIS_MANAGER("Business Analysis Manager", "BAM");

  private final String description;
  private final String code;

  EmployeePosition(String description, String code) {
    this.description = description;
    this.code = code;
  }

  private static final Map<String, EmployeePosition> lookupByCode = new HashMap<>();

  static {
    Stream.of(EmployeePosition.values()).forEach(ep -> lookupByCode.put(ep.code.toLowerCase(), ep));
  }

  public static EmployeePosition fromCode(String code){
    if(StringUtils.isEmpty(code)) {
      return null;
    }
    return lookupByCode.get(code.toLowerCase());
  }
}
