package com.example.employee.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter
@AllArgsConstructor @Builder
public class Vacation {
  private Long id;
  private String userName;
  private LocalDate startDate;
  private LocalDate endDate;
  private VacationRequestStatus status;
}
