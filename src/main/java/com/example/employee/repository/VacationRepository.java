package com.example.employee.repository;

import com.example.employee.domain.Vacation;
import com.example.employee.domain.VacationRequestStatus;

import java.util.List;

public interface VacationRepository {
  Vacation findById(Long id);

  List<Vacation> findBy(String userName);

  Vacation add(Vacation vacation);

  void updateStatus(Long id, VacationRequestStatus status);
}
