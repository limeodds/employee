package com.example.employee.repository.converter;

import com.example.employee.domain.Vacation;
import com.example.employee.domain.VacationRequestStatus;
import com.example.employee.repository.db.entity.VacationEntity;

public class VacationConverter {
  public static Vacation toDomain(VacationEntity dbObj) {
    if(dbObj == null) {
      return null;
    }

    return Vacation.builder()
                   .id(dbObj.getId())
                   .userName(dbObj.getUserName())
                   .startDate(dbObj.getStartDate())
                   .endDate(dbObj.getEndDate())
                   .status(VacationRequestStatus.valueOf(dbObj.getStatus()))
                   .build();
  }

  public static VacationEntity toDb(Vacation domainObj) {
    return VacationEntity.builder()
                   .id(domainObj.getId())
                   .userName(domainObj.getUserName())
                   .startDate(domainObj.getStartDate())
                   .endDate(domainObj.getEndDate())
                   .status(domainObj.getStatus().name())
                   .build();
  }
}
