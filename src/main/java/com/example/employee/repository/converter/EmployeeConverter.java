package com.example.employee.repository.converter;

import com.example.employee.repository.db.entity.EmployeeEntity;
import com.example.employee.domain.Employee;
import com.example.employee.domain.EmployeePosition;

public class EmployeeConverter {
  public static Employee toDomain(EmployeeEntity dbObj) {
    if(dbObj == null) {
      return null;
    }
    return Employee.builder()
                   .id(dbObj.getId())
                   .userName(dbObj.getUserName())
                   .firstName(dbObj.getFirstName())
                   .lastName(dbObj.getLastName())
                   .position(EmployeePosition.valueOf(dbObj.getPosition()))
                   .managerId(dbObj.getManagerId())
                   .totalDaysOff(dbObj.getTotalDaysOff())
                   .availableDaysOff(dbObj.getAvailableDaysOff())
                   .build();
  }

  public static EmployeeEntity toDb(Employee domainObj) {
    return EmployeeEntity.builder()
                         .id(domainObj.getId())
                         .userName(domainObj.getUserName())
                         .firstName(domainObj.getFirstName())
                         .lastName(domainObj.getLastName())
                         .position(domainObj.getPosition().name())
                         .managerId(domainObj.getManagerId())
                         .totalDaysOff(domainObj.getTotalDaysOff())
                         .availableDaysOff(domainObj.getAvailableDaysOff())
                         .build();
  }
}
