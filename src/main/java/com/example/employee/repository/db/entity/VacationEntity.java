package com.example.employee.repository.db.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "T_VACATION")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VacationEntity {
  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "USERNAME")
  private String userName;

  @Column(name = "START_DATE")
  private LocalDate startDate;

  @Column(name = "END_DATE")
  private LocalDate endDate;

  @Column(name = "STATUS")
  private String status;
}
