package com.example.employee.repository.db.impl;

import com.example.employee.domain.Vacation;
import com.example.employee.domain.VacationRequestStatus;
import com.example.employee.repository.VacationRepository;
import com.example.employee.repository.converter.VacationConverter;
import com.example.employee.repository.db.VacationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class VacationRepositoryDb implements VacationRepository {
  @Autowired
  private VacationDao vacationDao;

  @Override
  public Vacation findById(Long id) {
    return VacationConverter.toDomain(vacationDao.findById(id));
  }

  @Override
  public List<Vacation> findBy(String userName) {
    return vacationDao.findBy(userName).stream().map(VacationConverter::toDomain).collect(toList());
  }

  @Override
  public Vacation add(Vacation vacation) {
    return VacationConverter.toDomain(vacationDao.add(VacationConverter.toDb(vacation)));
  }

  @Override
  public void updateStatus(Long id, VacationRequestStatus status) {
    vacationDao.updateStatus(id, status.name());
  }
}
