package com.example.employee.repository.db.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "T_EMPLOYEE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeEntity {
  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "USERNAME")
  private String userName;

  @Column(name = "FIRST_NAME")
  private String firstName;

  @Column(name = "LAST_NAME")
  private String lastName;

  @Column(name = "POSITION")
  private String position;

  @Column(name = "MANAGER_ID")
  private Long managerId;

  @Column(name = "TOTAL_DAYS_OFF")
  private int totalDaysOff;

  @Column(name = "AVAILABLE_DAYS_OFF")
  private int availableDaysOff;
}
