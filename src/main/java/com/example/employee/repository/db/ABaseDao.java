package com.example.employee.repository.db;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

//@Transactional
public abstract class ABaseDao<T> {
  private final Class<T> clazz;

  @PersistenceContext
  private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  public ABaseDao() {
    this.clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
  }

  public T findById(Long id) {
    return entityManager.find(clazz, id);
  }

  @SuppressWarnings("unchecked")
  public List<T> findAll() {
    return entityManager.createQuery("from " + clazz.getName()).getResultList();
  }

  public T add(T entity) {
    entityManager.persist(entity);
    entityManager.flush();
    return entity;
  }

  public void update(T entity) {
    entityManager.merge(entity);
  }

  public void delete(T entity) {
    entityManager.remove(entity);
  }

  public void deleteById(Long entityId) {
    T entity = findById(entityId);
    delete(entity);
  }

  public List<T> findByNamedParameters(String query, Map<String, Object> params) {
    TypedQuery<T> typedQuery = entityManager.createQuery(query, clazz);

    params.forEach(typedQuery::setParameter);

    return typedQuery.getResultList();
  }

  public int update(String hqlUpdate, Map<String, Object> params) {
    Query query = entityManager.createQuery(hqlUpdate);

    params.forEach(query::setParameter);

    return query.executeUpdate();
  }
}
