package com.example.employee.repository.db.impl;

import com.example.employee.domain.Employee;
import com.example.employee.repository.EmployeeRepository;
import com.example.employee.repository.converter.EmployeeConverter;
import com.example.employee.repository.db.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepositoryDb implements EmployeeRepository {
  @Autowired
  private EmployeeDao employeeDao;

  @Override
  public Employee findBy(Long id) {
    return EmployeeConverter.toDomain(employeeDao.findById(id));
  }

  @Override
  public Employee findBy(String userName) {
    return EmployeeConverter.toDomain(employeeDao.findBy(userName));
  }

  @Override
  public Employee add(Employee employee) {
    return EmployeeConverter.toDomain(employeeDao.add(EmployeeConverter.toDb(employee)));
  }

  @Override
  public void update(Employee employee){
    employeeDao.update(EmployeeConverter.toDb(employee));
  }
}
