package com.example.employee.repository.db;

import com.example.employee.repository.db.entity.VacationEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class VacationDao extends ABaseDao<VacationEntity> {

  public List<VacationEntity> findBy(String userName) {
    String query = "from " + VacationEntity.class.getSimpleName() + " where userName = :userName";

    Map<String, Object> params = new HashMap<>();
    params.put("userName", userName);

    return findByNamedParameters(query, params);
  }

  public void updateStatus(Long id, String status) {
    VacationEntity entity = findById(id);
    entity.setStatus(status);

    update(entity);
  }

}
