package com.example.employee.repository.db;

import com.example.employee.repository.db.entity.EmployeeEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EmployeeDao extends ABaseDao<EmployeeEntity> {
  public EmployeeEntity findBy(String userName) {
    String query = "from " + EmployeeEntity.class.getSimpleName() + " where userName = :userName";

    Map<String, Object> params = new HashMap<>();
    params.put("userName", userName);

    List<EmployeeEntity> byNamedParameters = findByNamedParameters(query, params);
    if (CollectionUtils.isEmpty(byNamedParameters)) {
      return null;
    }

    return byNamedParameters.get(0);
  }
}
