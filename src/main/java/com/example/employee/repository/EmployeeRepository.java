package com.example.employee.repository;

import com.example.employee.domain.Employee;

public interface EmployeeRepository {
  Employee findBy(Long id);

  Employee findBy(String userName);

  Employee add(Employee employee);

  void update(Employee employee);
}
