package com.example.employee.controller;

import com.example.employee.domain.Employee;
import com.example.employee.domain.converter.EmployeeJsonConverter;
import com.example.employee.domain.json.EmployeeJson;
import com.example.employee.domain.json.EmployeeJsonInfo;
import com.example.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.net.URI;


@RestController
@RequestMapping("/v1/employees")
public class EmployeeController implements FindEntityOrThrowException {
  @Autowired
  private EmployeeService employeeService;

  @PostConstruct
  public void postConstruct() {
    System.out.println("nana");
  }

  @PostMapping(value = "")
  public ResponseEntity<?> addEmployee(@RequestBody EmployeeJson jsonObj) {
    Employee employee = EmployeeJsonConverter.toDomain(jsonObj);

    employeeService.add(employee);

    // Set the location header for the newly created resource
    HttpHeaders responseHeaders = new HttpHeaders();
    URI newPollUri = ServletUriComponentsBuilder.fromCurrentRequest()
                                                .path("/{username}")
                                                .buildAndExpand(employee.getUserName())
                                                .toUri();
    responseHeaders.setLocation(newPollUri);
    return new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
  }

  @GetMapping(value = "/{username}")
  public EmployeeJsonInfo getEmployee(@PathVariable("username") String userName) {
    Employee employee = findEmployee(userName, employeeService);

    EmployeeJsonInfo employeeInfo = EmployeeJsonConverter.toJsonInfo(employee);

    if (employee.getManagerId() != null) {
      Employee manager = employeeService.findBy(employee.getManagerId());
      employeeInfo.setManagerId(manager.getId());
      employeeInfo.setManagerUsername(manager.getUserName());
      employeeInfo.setManagerFullName(manager.getFullName());
    }

    return employeeInfo;
  }

  @GetMapping(value = "/{username}/available-days-off")
  public ResponseEntity<?> getEmployeeAvailableDaysOff(@PathVariable("username") String username) {
    Employee employee = findEmployee(username, employeeService);

    return new ResponseEntity<>(employee.getAvailableDaysOff(), HttpStatus.OK);
  }

  @GetMapping(value = "/{username}/used-days-off")
  public ResponseEntity<?> getEmployeeUsedDaysOff(@PathVariable("username") String username) {
    Employee employee = findEmployee(username, employeeService);

    return new ResponseEntity<>(employee.getTotalDaysOff() - employee.getAvailableDaysOff(), HttpStatus.OK);
  }
}
