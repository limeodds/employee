package com.example.employee.controller;

import com.example.employee.controller.exception.ResourceNotFoundException;
import com.example.employee.domain.Employee;
import com.example.employee.domain.Vacation;
import com.example.employee.service.EmployeeService;
import com.example.employee.service.VacationService;

public interface FindEntityOrThrowException {
  default Vacation findVacation(Long id, VacationService vacationService) {
    Vacation vacation = vacationService.findById(id);

    if (vacation == null) {
      throw new ResourceNotFoundException("Vacation '" + id + "' not found");
    }

    return vacation;
  }

  default Employee findEmployee(String username, EmployeeService employeeService) {
    Employee employee = employeeService.findBy(username);

    if (employee == null) {
      throw new ResourceNotFoundException("Employee '" + username + "' not found");
    }

    return employee;
  }
}
