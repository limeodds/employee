package com.example.employee.controller;

import com.example.employee.controller.exception.BadRequestException;
import com.example.employee.controller.exception.PreconditionFailedException;
import com.example.employee.domain.DateUtils;
import com.example.employee.domain.Employee;
import com.example.employee.domain.Vacation;
import com.example.employee.domain.VacationRequestStatus;
import com.example.employee.domain.converter.VacationJsonConverter;
import com.example.employee.domain.json.VacationJson;
import com.example.employee.externalapi.LegalDayOffServiceExtImpl;
import com.example.employee.externalapi.model.DaysNo;
import com.example.employee.service.EmployeeService;
import com.example.employee.service.VacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/v1/vacations")
public class VacationController implements FindEntityOrThrowException {
  @Autowired
  private VacationService vacationService;
  @Autowired
  private LegalDayOffServiceExtImpl legalDayOffService;
  @Autowired
  private EmployeeService employeeService;

  @PostMapping(value = "")
  public ResponseEntity<?> addVacation(@RequestBody VacationJson jsonObj) {
    String userName = jsonObj.getUserName();

    Employee employee = findEmployee(userName, employeeService);

    if (employee.getAvailableDaysOff() == 0) {
      throw new PreconditionFailedException("Employee '" + userName + "' has no more vacation to take. Back to work");
    }

    LocalDate startDate = toLocalDate(jsonObj.getStartDate());
    LocalDate endDate = toLocalDate(jsonObj.getEndDate());

    DaysNo daysNo = legalDayOffService.getDaysNo(startDate, endDate);

    if (employee.getAvailableDaysOff() < daysNo.getWorkingDaysNo()) {
      throw new PreconditionFailedException("Employee '" + userName + "' can take a maximum of " + employee.getAvailableDaysOff() + " days off."
                                            + " You requested for " + daysNo.getWorkingDaysNo());
    }

    Vacation vacation = Vacation.builder()
                                .userName(userName)
                                .startDate(startDate)
                                .endDate(endDate)
                                .status(VacationRequestStatus.VALIDATED)
                                .build();

    Vacation addedObj = vacationService.add(vacation);

    //TODO notify manager (send email)

    // Set the location header for the newly created resource
    HttpHeaders responseHeaders = new HttpHeaders();
    URI newPollUri = ServletUriComponentsBuilder.fromCurrentRequest()
                                                .path("/{id}")
                                                .buildAndExpand(addedObj.getId())
                                                .toUri();
    responseHeaders.setLocation(newPollUri);
    return new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
  }

  @GetMapping(value = "/{id}")
  public VacationJson getVacation(@PathVariable("id") Long id) {
    Vacation vacation = findVacation(id, vacationService);
    return VacationJsonConverter.toJson(vacation);
  }

  @GetMapping(value = "/username/{username}")
  public List<VacationJson> getVacations(@PathVariable("username") String username) {
    return vacationService.findBy(username).stream()
                          .sorted(Comparator.comparing(Vacation::getStartDate))
                          .map(VacationJsonConverter::toJson)
                          .collect(toList());
  }

  @PutMapping(value = "/{id}")
  public ResponseEntity<?> updateVacationRequestStatus(@RequestBody VacationJson vacationJson, @PathVariable("id") Long vacationId) {
    LocalDate startDate = toLocalDate(vacationJson.getStartDate());
    LocalDate endDate = toLocalDate(vacationJson.getEndDate());

    DaysNo daysNo = legalDayOffService.getDaysNo(startDate, endDate);

    VacationRequestStatus requestStatus = VacationRequestStatus.valueOf(vacationJson.getStatus());
    vacationService.updateStatus(vacationId, requestStatus);

    if (requestStatus == VacationRequestStatus.APPROVED) {
      Employee employee = findEmployee(vacationJson.getUserName(), employeeService);

      employee.setAvailableDaysOff(employee.getAvailableDaysOff() - daysNo.getWorkingDaysNo());

      employeeService.update(employee);
    }

    //TODO notify Employee (send email)

    return new ResponseEntity<>(HttpStatus.OK);
  }

  private LocalDate toLocalDate(String value) {
    try {
      return DateUtils.toLocalDate(value);
    } catch (DateTimeParseException e) {
      throw new BadRequestException("Invalid date format: '" + value + "'. Must be " + DateUtils.dateTimePattern);
    }
  }
}
