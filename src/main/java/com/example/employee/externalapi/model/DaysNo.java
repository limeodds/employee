package com.example.employee.externalapi.model;

import lombok.*;

@Getter @Setter @NoArgsConstructor
@AllArgsConstructor @Builder
@ToString
public class DaysNo {
  private int legalDaysOffNo;
  private int workingDaysNo;
}
