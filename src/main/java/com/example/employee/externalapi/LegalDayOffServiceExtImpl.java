package com.example.employee.externalapi;

import com.example.employee.domain.DateUtils;
import com.example.employee.externalapi.model.DaysNo;
import com.example.employee.service.LegalDayOffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Service
public class LegalDayOffServiceExtImpl implements LegalDayOffService {
  @Value("${ext.api.endpoint.legal-day-off.days-no}")
  private String daysNoEndpoint;

  @Autowired
  private RestTemplate restTemplate;

  @Override
  public DaysNo getDaysNo(LocalDate startDate, LocalDate endDate) {
    String endPoint = String.format(daysNoEndpoint, DateUtils.toString(startDate), DateUtils.toString(endDate));

    return restTemplate.getForObject(endPoint, DaysNo.class);
  }
}
